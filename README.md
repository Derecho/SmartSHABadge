# Smart SHABadge
SHA2017 badge application to expose the screen and LEDs over MQTT.

## Features
- [x] Display notifications received on `shabadge/notification` on the e-ink
      screen.
- [ ] Control the six RGB LEDs on the badge as one smart light
- [ ] Control the six RGB LEDs on the badge as individual smart lights

## Installation
This code has not yet been pushed to the [hatchery](https://badge.sha2017.org/).
You can install the `SDcard` application which is in the hatchery, and then
place the contents of this repository in a folder called `sdcard`, on a microSD
card.

## Acknowledgements
Used the following other programs as an example/reference:

* [EasyMQTT](https://badge.sha2017.org/files/15262)

