import gc
import machine
import utime
import easywifi
import easydraw
import badge
import ugfx
import dialogs
import binascii
from umqtt.simple import MQTTClient

MQTT_NOTIF_TOPIC = b"shabadge/notification"
MQTT_RESET_TOPIC = b"shabadge/reset"

notification_buffer = []  # [[time1, msg1], [time2, msg2]]

# Initial stuff
# Make sure we're on wifi
easydraw.msg("Connecting to WiFi", title='Still Connecting Anyway...', reset=True)
easywifi.enable(True)
if easywifi.state == False:
    easydraw.msg("Unable to connect to network")
    easydraw.msg("Waiting 5 seconds to try again!")
    badge.eink_busy_wait()
    machine.deepsleep(5000)

# Find out what MQTT server to connect to
server = badge.nvs_get_str("smart_shabadge", "server", None)
new_server = dialogs.prompt_text("MQTT Server", server)
if new_server != server:
    print("[D] Saving new server")
    badge.nvs_set_str("smart_shabadge", "server", new_server)
    server = new_server

# Updates notifications displayed on the screen
def update_notifications():
    global notification_buffer
    notifications = len(notification_buffer)

    ugfx.area(0,30, 296, 98, ugfx.WHITE)
    # Most recent message
    if notifications > 0:
        # Time first
        ugfx.string(5, 30, notification_buffer[-1][0], "Roboto_Regular12", ugfx.BLACK)
        # One or two msg lines
        ugfx.string(5, 45, notification_buffer[-1][1][:30], "Roboto_Regular22", ugfx.BLACK)
        if len(notification_buffer[-1][1]) > 30:
            ugfx.string(5, 70, notification_buffer[-1][1][30:60], "Roboto_Regular22", ugfx.BLACK)
    # Previous message
    if notifications > 1:
        ugfx.string(5, 100, "{}: {}".format(notification_buffer[-2][0], notification_buffer[-2][1][:60]), "Roboto_Regular12", ugfx.BLACK)
    ugfx.flush(ugfx.LUT_FULL)

# Received messages from subscriptions will be delivered to this callback
def sub_cb(topic, msg):
    print("Received {} on topic {}".format(msg, topic))

    if topic == MQTT_NOTIF_TOPIC:
        global notification_buffer
        (year, month, mday, hour, minute, second, weekday, yearday) = utime.localtime()
        if len(notification_buffer) > 1:
            notification_buffer = notification_buffer[1:]
        notification_buffer.append(["{:02d}:{:02d}".format(hour, minute), msg.decode("utf-8")])
        update_notifications()

    elif topic == MQTT_RESET_TOPIC:
        machine.reset()

def main(server):
    clientname = "SHABadge_{}".format(binascii.hexlify(machine.unique_id()).decode("ascii"))

    print("Connecting to MQTT as {}".format(clientname))
    easydraw.msg("Connecting to MQTT...")
    c = MQTTClient(clientname, server)
    c.set_callback(sub_cb)
    c.connect()
    c.subscribe(b"shabadge/#")
    
    # Clear screen properly
    ugfx.clear(ugfx.BLACK)
    ugfx.flush()
    badge.eink_busy_wait()
    ugfx.clear(ugfx.WHITE)
    ugfx.flush()

    easydraw.msg("MQTT Connected", title="Smart SHABadge", reset=True)
    c.check_msg()
    while True:
        gc.collect()
        c.check_msg()
        utime.sleep(1)
    c.disconnect()

main(server)
